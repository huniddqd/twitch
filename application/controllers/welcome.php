<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('message');
		$this->load->model('welcome_model');
		$this->load->model('stream_model');
		$this->load->helper('form');
		$this->load->library('message');
	}

	public function index()
	{	
		$data = array('message' => '' );
		$data['title'] = "HSC - Kezdőlap";
		$message = new Message();
		if ($_POST){
			$data['message'] = $this->router();
		}
		if(!$this->input->is_ajax_request()){
			$this->load->view('header', $data);
			$this->load->view('welcome_message', $data);
		}
	}

	private function router()
	{
		if ($this->input->post('stream')) {
			return $this->add_stream();
		}		
	}

	private function add_stream(){
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('stream', 'Stream', 'required|alpha_dash|is_unique[streams.name]');

		$message = new Message;

		if ($this->form_validation->run() == FALSE) {
			$message->setType("danger");
			$message->setMessage(str_replace(array("\r","\n"), '', validation_errors()));
			if ($this->input->is_ajax_request()) {
				$temp = array('type' => 'danger', 'message' => validation_errors());
				echo json_encode($temp);
			}
			return $message;
		} else {
			$message->setType("success");
			$message->setMessage("A streamet sikeresen hozzáadtuk az adatbázishoz.");
			$this->stream_model->add_stream();
			if ($this->input->is_ajax_request()) {
				$temp = array('type' => "success", 'message' =>"A streamet sikeresen hozzáadtuk az adatbázishoz.");
				echo json_encode($temp);
			}
			
		}
		return $message;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
