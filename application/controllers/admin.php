<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('stream_model');
		$this->load->library('form_validation');
	}

	public function index()	{
		$this->form_validation->set_rules('username', 'Felhasználó név', 'trim|required|min_length[5]|max_length[12]|xss_clean');
		$this->form_validation->set_rules('password', 'Jelszó', 'trim|required|xss_clean');
		if(($this->input->post('username') && $this->input->post('password') && $this->admin_model->login($this->input->post('username'),$this->input->post('password')))|| $this->session->userdata('logged_in') == TRUE){
			$array = array(
				'username' => $this->input->post('username'),
				'logged_in' => TRUE
			);
			
			$this->session->set_userdata( $array );
			$this->admin_view();
			
		}else{
			$this->load->view('adminlogin_view');
		}
	}

	private function admin_view()
	{
		$data['all_stream'] = $this->stream_model->list_all();
		$this->load->view('admin_view', $data);
	}

	public function edit($change, $stream_name)
	{
		switch ($change) {
			case 'delete':
				$this->stream_model->delete_stream($stream_name);
				$this->admin_view();
				break;

			case 'validate':
				$this->stream_model->validate_stream($stream_name);
				$this->admin_view();
				break;
			
			default:
				$this->admin_view();
				break;
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('base_url()', 'refresh');
	}
}
/* End of file admin.php */
/* Location: ./application/controllers/admin.php */
?>