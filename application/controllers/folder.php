<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Folder extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('stream_model');
		$this->load->helper('twitch');
		$this->load->library('message');
	}

	public function index()
	{
		$data = null;
		$data['title'] = "HSC - Játékok";
		$games = array();
		$data['count_by_games'] = array();
		$data['error'] = false;
		$i = 0;
		$j = 0;

		$online_streams = get_online($this->stream_model->list_valid())->streams;
		foreach ($online_streams as $temp) {
			$games[$i] = $temp->channel->game;
			$i++;
		}

		sort($games);
		$data['count_by_games'][$j]['db'] = 1;
		if(count($games) != 0){
			if (count($games) == 1) {
				$data['count_by_games'][$j]['name'] = $games[0];				
			}else{
				for($i = 0; $i < count($games)-1; $i++){
					if($games[$i] == $games[$i+1]){
						$data['count_by_games'][$j]['db']++;
					}else{
						$data['count_by_games'][$j]['name'] = $games[$i];
						$j++;
						$data['count_by_games'][$j]['db'] = 1;
					}
				}
				$data['count_by_games'][$j]['name'] = $games[$i];
			}
		}else{
			$data['error'] = true;
		}

		for($i=0; $i < count($data['count_by_games']); $i++){
			$data['count_by_games'][$i]['image'] = get_game_image($data['count_by_games'][$i]['name']);
			$data['count_by_games'][$i]['url'] = preg_replace('#[^\w()/.%\-&]#',"", $data['count_by_games'][$i]['name']);
		}

		foreach ($data['count_by_games'] as $val)
			$tmp_age[] = $val['db'];

		foreach ($data['count_by_games'] as $val)
			$tmp_name[] = $val['name'];

		array_multisort($tmp_age, SORT_DESC, $tmp_name, $data['count_by_games']);


		$this->load->view('header', $data);
		$this->load->view('games_view', $data);
	}

	public function games()
	{
		$game = $this->input->get('game');
		$data['error'] = false;


		$data['title'] = "HSC - " . $game;

		if ($game == ''){
			redirect(base_url() ."index.php/folder/", 'refresh');
		}
		$data['online_streams'] = get_online($this->stream_model->list_valid(), $game)->streams;

		if(empty($data['online_streams'])){
			$data['error'] = true;
		}


		$this->load->view('header', $data);
		$this->load->view('channels_view', $data);
	}

	public function channel($channel_name)
	{
		echo $channel_name;
	}

}

/* End of file directory.php */
/* Location: ./application/controllers/directory.php */