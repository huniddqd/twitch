<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('TwitchClientId', 'Client-ID: s8tk3513ihvdci0acdz4gqkzj555711');
define('TwitchApiVersion', 'ACCEPT: application/vnd.twitchtv.v3+json');

if ( ! function_exists('get_online'))
{
	function get_online($streams = '', $game = '')
	{
		$header = array( TwitchApiVersion, TwitchClientId, 'Content-type: application/json');
		$request = "";
		if($game != ''){
			$request = $request . "game=" . urlencode($game) . "&";
		}
		$request = $request . "channel=";
		foreach ($streams as $temp) {
			$request = $request . strtolower($temp['name']) . ",";
		}
		//$request = strtolower($request);

		$request = "https://api.twitch.tv/kraken/streams?" . $request;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_URL, $request);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$ret = curl_exec($ch);
		curl_close($ch);


		return json_decode($ret);
	}
}

if ( ! function_exists('get_game_image'))
{
	function get_game_image($game = '')
	{
		$done = false;
		$ci =& get_instance();
		$ci->load->helper('file');
		$gamerep = preg_replace('#[^\w()/.%\-&]#',"", $game);

		$localimg = (get_filenames('application/img/games/'));

		foreach ($localimg as $key) {
			if(substr($key, 0, -4) == $gamerep){
				$done = true;
				return base_url() . 'application/img/games/' . $gamerep . '.jpg';
			}
		}

		if($done == false){
			$url = "";
			$header = array( TwitchApiVersion, TwitchClientId, 'Content-type: application/json');

			$game_encoded = strtolower(urlencode($game));

			$request = "https://api.twitch.tv/kraken/search/games?type=suggest&q=" . $game_encoded;


			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_URL, $request);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$ret = curl_exec($ch);
			curl_close($ch);

			$ret = json_decode($ret);

			foreach ($ret->games as $key) {
				if($key->name == $game){
					copy($key->box->large, 'application/img/games/' . $gamerep . '.jpg');
					return $key->box->large;
				}
			}
		}
	}
}