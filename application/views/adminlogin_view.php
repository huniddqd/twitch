<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Admin login</title>
</head>

<body>
	<form class="form-horizontal" action="<?php echo base_url();?>/index.php/admin" method="POST">
		<fieldset>

			<!-- Form Name -->
			<legend>Belépés</legend>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="username">Felhasználó név</label>  
				<div class="col-md-4">
					<input id="username" name="username" type="text" placeholder="Felhasználó név" class="form-control input-md" required="">
					<span class="help-block">Felhasználó név</span>  
				</div>
			</div>

			<!-- Password input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="password">Jelszó</label>
				<div class="col-md-4">
					<input id="password" name="password" type="password" placeholder="Jelszó" class="form-control input-md" required="">
					<span class="help-block">Jelszó</span>
				</div>
			</div>

			<!-- Button -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="login"></label>
				<div class="col-md-4">
					<button id="login" name="login" class="btn btn-default" type="submit">Belépés</button>
				</div>
			</div>

		</fieldset>
	</form>

</body>

</html>