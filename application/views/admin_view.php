<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Bootstrap 101 Template</title>
<link href="<?php echo base_url();?>/application/css/custom.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="<?php echo base_url();?>/application/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

  </head>
  <body>
      <div class="page-header">
          <h1>Admin</h1>
      </div>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs">
          <li class="active"><a href="#all" data-toggle="tab">Összes</a></li>
          <li><a href="#valid" data-toggle="tab">Elfogadott</a></li>
          <li><a href="#waiting" data-toggle="tab">Várakozik</a></li>
          <li><a href="#deleted" data-toggle="tab">Törölt</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="all">
            <?php
            foreach ($all_stream as $stream) {
                echo    "
                <div class=\"panel panel-default\">                    
                    <div class=\"panel-heading\">
                        <h3 class=\"panel-title\">" . $stream['name'] . "</h3>
                    </div>
                    <div class=\"panel-body\">
                        <a href=\"http://twitch.tv/" . $stream['name'] . "\" target=\"_blank\">http://http://twitch.tv/" . $stream['name'] . "</a>
                        <a href=\" " . base_url() . "index.php/admin/edit/validate/" . $stream['name'] . "\"><span class=\"glyphicon glyphicon-ok\"></span></a>
                        <a href=\"" . base_url() . "index.php/admin/edit/delete/" . $stream['name'] . "\"><span class=\"glyphicon glyphicon-remove\"></span></a>
                    </div>
                </div>
                ";
            }
            ?>
        </div>
        <div class="tab-pane" id="valid"> 
            <?php
            foreach ($all_stream as $stream) {
                if($stream['valid']== TRUE){
                    echo    "
                    <div class=\"panel panel-default\">                    
                        <div class=\"panel-heading\">
                            <h3 class=\"panel-title\">" . $stream['name'] . "</h3>
                        </div>
                        <div class=\"panel-body\">
                            <a href=\"http://twitch.tv//" . $stream['name'] . "\" target=\"_blank\">http://twitch.tv/" . $stream['name'] . "</a>
                            <a href=\"" . base_url() . "index.php/admin/edit/validate/" . $stream['name'] . "\"><span class=\"glyphicon glyphicon-ok\"></span></a>
                            <a href=\"" . base_url() . "index.php/admin/edit/delete/" . $stream['name'] . "\"><span class=\"glyphicon glyphicon-remove\"></span></a>
                        </div>
                    </div>
                    ";
                }
            }
            ?>
        </div>
        <div class="tab-pane" id="waiting">
            <?php
            foreach ($all_stream as $stream) {
                if($stream['valid'] == False && $stream['deleted'] == FALSE){
                    echo    "
                    <div class=\"panel panel-default\">                    
                        <div class=\"panel-heading\">
                            <h3 class=\"panel-title\">" . $stream['name'] . "</h3>
                        </div>
                        <div class=\"panel-body\">
                            <a href=\"http://twitch.tv/" . $stream['name'] . "\" target=\"_blank\">http://twitch.tv/" . $stream['name'] . "</a>
                            <a href=\"" . base_url() . "index.php/admin/edit/validate/" . $stream['name'] . "\"><span class=\"glyphicon glyphicon-ok\"></span></a>
                            <a href=\"" . base_url() . "index.php/admin/edit/delete/" . $stream['name'] . "\"><span class=\"glyphicon glyphicon-remove\"></span></a>
                        </div>
                    </div>
                    ";
                }
            }
            ?>
        </div>
        <div class="tab-pane" id="deleted">
            <?php
            foreach ($all_stream as $stream) {
                if($stream['deleted']== TRUE){
                    echo    "
                    <div class=\"panel panel-default\">                    
                        <div class=\"panel-heading\">
                            <h3 class=\"panel-title\">" . $stream['name'] . "</h3>
                        </div>
                        <div class=\"panel-body\">
                            <a href=\"http://twitch.tv/" . $stream['name'] . "\" target=\"_blank\">http://twitch.tv/" . $stream['name'] . "</a>
                            <a href=\"" . base_url() . "index.php/admin/edit/validate/" . $stream['name'] . "\"><span class=\"glyphicon glyphicon-ok\"></span></a>
                            <a href=\"" . base_url() . "index.php/admin/edit/delete/" . $stream['name'] . "\"><span class=\"glyphicon glyphicon-remove\"></span></a>
                        </div>
                    </div>
                    ";
                }
            }
            ?>
        </div>
    </div>

    <a href="/<?php echo base_url();?>/index.php/admin/logout">Kijelentkezés</a>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>/application/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>/application/js/jquery.bootstrap-growl.min.js"></script>
    <script src="<?php echo base_url();?>/application/js/custom.js"></script>
</body>
</html>