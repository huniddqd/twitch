<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title;?></title>
	<link href="<?php echo base_url();?>application/css/custom.css" rel="stylesheet">
	<!-- Bootstrap -->
	<link href="<?php echo base_url();?>application/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

  </head>
  <body>
  	<nav class="navbar-inverse" role="navigation">
  		<div class="container-fluid">
  			<div class="navbar-header">
  				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
  					<span class="sr-only">Toggle navigation</span>
  					<span class="icon-bar"></span>
  					<span class="icon-bar"></span>
  					<span class="icon-bar"></span>
  				</button>
  				<div class="row">
  					<div class="col-xs-1 col-sm-1 col-md-1">
  						<a class="navbar-brand" href="<?php echo base_url();?>index.php">HSC</a>
  					</div>
  					<div class="col-xs-9 col-sm-3 col-md-3 center-block">
  						<div class="input-group pull-center" id="searchbar">
  							<input type="text" class="navbar-form form-control" id="searchbar_in" placeholder="Keresés">
  							<span class="input-group-btn">
  								<button class="btn btn-default navbar-btn" type="button"><span class="glyphicon glyphicon-search"></span></button>
  							</span>
  						</div><!-- /input-group -->

  					</div>
  					<div class="col-xs-1 col-sm-8 col-md-8">
  						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  							<ul class="nav navbar-nav pull-right">
  								<li><a href="<?php echo base_url();?>index.php/folder/">Játékok</a></li>
                  <li><a href="<?php echo base_url();?>index.php/streams/">Streamek</a></li>
  								<li><a href="#">Bejelentkezés</a></li>
  								<li><a href="#">Regisztráció</a></li>
  							</ul>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</nav>