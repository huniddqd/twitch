  	<div id="myCarousel" class="carousel slide hidden-xs" data-ride="carousel">
  		<!-- Indicators -->
  		<ol class="carousel-indicators">
  			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
  			<li data-target="#myCarousel" data-slide-to="1"></li>
  			<li data-target="#myCarousel" data-slide-to="2"></li>
  		</ol>
  		<div class="carousel-inner">
  			<div class="item active">
  				<img src="<?php echo base_url(); ?>application/img/tempb.png">
  				<div class="container">
  					<div class="carousel-caption">
  						<div class="row">
  							<div class="col-xs-7" id="streampicture"><img class="img-responsive" src="<?php echo base_url(); ?>application/img/temp.jpg"></div>
  							<div class="col-xs-5">
  								<h1>Example headline.</h1>
  								<p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
  								<p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
  			<div class="item">
  				<img src="<?php echo base_url(); ?>application/img/tempb.png">
  				<div class="container">
  					<div class="carousel-caption">
  						<div class="row">
  							<div class="col-xs-7" id="streampicture"><img class="img-responsive" src="<?php echo base_url(); ?>application/img/temp.jpg"></div>
  							<div class="col-xs-5">
  								<h1>Example headline.</h1>
  								<p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
  								<p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
  			<div class="item">
  				<img src="<?php echo base_url(); ?>application/img/tempb.png">
  				<div class="container">
  					<div class="carousel-caption">
  						<div class="row">
  							<div class="col-xs-7" id="streampicture"><img class="img-responsive" src="<?php echo base_url(); ?>application/img/temp.jpg"></div>
  							<div class="col-xs-5">
  								<h1>Example headline.</h1>
  								<p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
  								<p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  		<a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
  		<a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
  	</div><!-- /.carousel -->

  	<div class="row" >
  		<div class="col-xs-12" id="description">
  			<div class="jumbotron" id="addStream">
  				<div class="container">
  					<h1>Stream hozzáadása:</h1>
  					<p>Ha tudsz egy magyar streamről amit még nem tartalmaz az adatbázisunk, vagy a saját streamedet szeretnéd hozzáadni az oldalhoz akkor kérlek írd be a streamhez tartozó twitch-urlt az alábbi mezőbe és küld el nekünk.</p><p> Az elküldött streamek nem kerülnek be egyből az adatbázisba, előtte ellenőrizzük, hogy az adott stream megfelel-e az oldal szabályzatának.</p>
  					<form method="POST" id="form_addStream" action="<?php echo base_url(); ?>" class="form-horizontal">
  						<div class="input-group">
  							<input type="text" id="stream" name="stream" class="form-control" placeholder="példastream">
  							<span class="input-group-btn">
  								<button class="btn btn-default" type="submit">Elküldés!</button>
  							</span>  							
  						</div><!-- /input-group -->
  					</form>
  				</div>
  			</div>
  		</div>
  	</div> <!-- end description-->

  	<footer class="row">
  		<div class="container">
  			<div class="col-md-7">
  				<p>A Lorem Ipsum egy egyszerû szövegrészlete, szövegutánzata a betûszedõ és nyomdaiparnak. A Lorem Ipsum az 1500-as évek óta standard szövegrészletként szolgált az iparban; mikor egy ismeretlen nyomdász összeállította a betûkészletét és egy példa-könyvet vagy szöveget nyomott papírra, ezt használta. Nem csak 5 évszázadot élt túl, de az elektronikus betûkészleteknél is változatlanul megmaradt. Az 1960-as években népszerûsítették a Lorem Ipsum részleteket magukbafoglaló Letraset lapokkal, és legutóbb softwarekkel mint például az Aldus Pagemaker.</p>
  			</div>
  			<div class="col-md-5">
  				<nav class="navbar-inverse" role="navigation">
  					<ul class="nav navbar-nav pull-right">
  						<li><a href="<?php echo base_url();?>index.php/fodler/">Játékok</a></li>
  						<li><a href="#">Bejelentkezés</a></li>
  						<li><a href="#">Regisztráció</a></li>
  						<li><a href="#">Kapcsolat</a></li>
  					</ul>
  				</nav>
  			</div>
  		</div>
  	</footer>


  	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  	<!-- Include all compiled plugins (below), or include individual files as needed -->
  	<script src="<?php echo base_url(); ?>application/js/bootstrap.min.js"></script>
  	<script src="<?php echo base_url(); ?>application/js/jquery.bootstrap-growl.min.js"></script>
  	<script src="<?php echo base_url(); ?>application/js/custom.js"></script>
  	<?php 
  	if (is_object($message)) {
  		echo "<script type=\"text/javascript\">message(\"" . $message->getMessage() . "\", \"" . $message->getType() ."\");</script>\n";
  	}
  	?>

  	<script type="application/javascript">
  		$(document).ready(function() {
  			$('#form_addStream').submit(function() {
  				event.preventDefault();
  				var form_data = {
  					stream : $('#stream').val(),
  					ajax : '1'
  				};
  				$.ajax({
  					url: "<?php echo base_url(); ?>index.php/welcome",
  					type: 'POST',
  					async : false,
  					data: form_data,
  					success: function(msg) {
  						var test = jQuery.parseJSON(msg);
  						message(test.message, test.type);
  					}
  				});
  				event.preventDefault();
  				return false;
  			});
  		});
  	</script>

  </body>
  </html>