<div class="page-header">
	<h1>Jelenleg nézhető játékok</h1>
</div>
<div class="row">
	<?php
	if($error == true){
		echo "<div class=\"alert alert-warning\">Nincs elérhető stream a ketegóriában jelenleg.</div>";
	}else{
		foreach ($online_streams as $key) {
			echo "<div class=\"col-xs-6 col-md-3 col-lg-2\">";
			echo "<a href=\"" . base_url() . "index.php/folder/channel/" . $key->channel->name . "\" class=\"thumbnail\">";
			echo "<span class=\"badge pull-right\">" . $key->channel->views . "</span>";
			echo $key->channel->display_name . "<br /><img src=\"" . $key->preview->large . "\" alt=\"" . "\" class=\"img-responsive img-thumbnail\">";
			echo $key->channel->status;
			echo "</a>";
			echo "</div>";
		}
	}
		?>
</div>