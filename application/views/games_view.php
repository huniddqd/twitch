<div class="page-header">
	<h1>Jelenleg nézhető játékok</h1>
</div>
<div class="row">
	<?php
	if($error == true){
		echo "<div class=\"alert alert-warning\">Nincs elérhető stream jelenleg.</div>";
	}else{
		foreach ($count_by_games as $key) {
			echo "<div class=\"col-xs-6 col-md-3 col-lg-2\">";
			echo "<a href=\"" . base_url() . "index.php/folder/games/?game=" . urlencode($key['name']) ."\" class=\"thumbnail\">";
			echo "<span class=\"badge pull-right\">" . $key['db']. "</span>";
			echo $key['name'] . "<br /><img src=\"" . $key['image']. "\" alt=\"" . $key['name']. "\" class=\"img-responsive img-thumbnail\">";
			echo "</a>";
			echo "</div>";
		}
	}
		?>
</div>