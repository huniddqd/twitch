<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Message {
	private $type;
	private $string;	

	public function create(){
		$this->type = "";
		$this->string = "";
	}
	public function setType($value='')
	{
		$this->type = $value;
	}
	public function setMessage($value='')
	{
		$this->string = $value;
	}

	public function getMessage(){
		return $this->string;

	}
	
	public function getType(){
		return $this->type;

	}
}

/* End of file Message.php */