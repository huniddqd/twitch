<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Stream_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  public function add_stream(){
    $data = array(
      'name' => $this->input->post('stream'),
      'cathegory' => 1
      );

    $this->db->insert('streams', $data);
  }

  public function delete_stream($stream_name)
  {
    $data = array(
     'deleted' => true,
     'valid' => false
     );
    $this->db->where('name', $stream_name);
    $this->db->update('streams', $data); 
  }

  public function validate_stream($stream_name)
  {
    $data = array(
     'valid' => true,
     'deleted' => false
     );
    $this->db->where('name', $stream_name);
    $this->db->update('streams', $data); 
  }

  public function list_all()
  {
    return $this->db->get('streams')->result_array();
  }

  public function list_valid()
  {
    return $this->db->get_where('streams', array('valid' => true))->result_array();
  }

  public function list_deleted()
  {
    return $this->db->get_where('streams', array('deleted' => true))->result_array();
  }

}
?>
