<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Admin_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  public function login($username, $password)
  {
    $query = $this->db->get_where('users', array('username' => $username), 1);
    foreach ($query->result() as $row){
      if(md5($password . $row->salt) === $row->password){
        return true;
      }else{
        return false;
      }
    }
    return false;
  }
}
?>
